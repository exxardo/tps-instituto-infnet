lista = []

print('''Para MOSTRAR tecle a
Para INCLUIR tecle b
Para REMOVER tecle c
Para STATUS DA OPERAÇÃO tecle d
Para LIMPAR e SAIR tecle s''')
    
while True:
    decisao = input('O que deseja fazer? ').upper()
    if decisao == 'A':
        print(lista)

    elif decisao == 'B':
        elemento = input('O que deseja incluir? ')
        lista.append(elemento)
        
    elif decisao == 'C':
        elemento = input('O que deseha remover? ')
        if elemento in lista:
            lista.remove(elemento)
        
        else:
            print('Não consta na lista')
        
    elif decisao == 'D':
        if elemento in lista:
            print(f'SUCESSO! O elemento {elemento} foi adicionado à lista.')
        
    elif decisao == 'S':
        lista.clear()
        break
    
    else:
        print('Tente uma das alternativas')
