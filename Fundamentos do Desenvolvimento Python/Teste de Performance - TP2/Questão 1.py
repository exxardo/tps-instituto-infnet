lista = []

for elemento in range(5):
    lista.append(elemento + 1)

print(lista)

if 3 in lista:
    lista.remove(3)
    print(lista)
if 6 in lista:
    lista.remove(6)
    print(lista)
else:
    print('O elemento "6" não está presente na lista.')

print('\nA lista contém', len(lista), 'elementos\n')

lista[3] = 6

print(lista)