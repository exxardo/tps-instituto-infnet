# Função para girar texto de acordo com a quantidade de casas
# desejada pelo usuário.
def texto_girar(casas_girar, texto):

    tamanho = len(texto)
    if casas_girar == tamanho:
        resultado = texto[::-1]

        # Saída
        print(f'\nResultado: {resultado}')

    else:
        if casas_girar > tamanho:
            casas_girar = casas_girar % tamanho

        inicio = texto[casas_girar:]
        final = texto[:casas_girar]
        resultado = inicio + final

        # Saída
        print(f'\nResultado: {resultado}')

# Receptor de dados do usuário.
casas_girar = int(input('Posições a rotacionar para a esquerda: '))
texto = input('Insira o texto que deseja rotacionar: ')

texto_girar(casas_girar, texto)
