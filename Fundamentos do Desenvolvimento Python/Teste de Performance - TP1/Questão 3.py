#Calculo fatorial do valor n
def calculo_fatorial(valor_n):
    resultado = 1
    for contador in range(1, valor_n + 1):
        resultado *= contador

    print(f'\nO fatorial de {valor_n} é: {resultado}')

#Receptor do dado do usuário
valor_n = int(input('Digite o valor de n: '))

if valor_n >= 0:
    calculo_fatorial(valor_n)
else:
    print('\nNão é possível calcular seu fatorial.')
