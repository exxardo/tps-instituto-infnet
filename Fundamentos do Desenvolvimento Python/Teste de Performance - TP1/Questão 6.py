#Função para definir o tipo de triângulo
def tipo_triangulo(tamanho_lado):
    
    x, y, z = tamanho_lado[0], tamanho_lado[1], tamanho_lado[2]
    
    #Definição do tipo de triângulo
    if x < y + z and y < x + z and z < x + y:
        if x == y and y == z:
            retorno_tringulo = '\nOs lados fornecidos são de um equilátero.'
        elif (x == y and x != z) or (x != y and x == z) or (x != z and y == z):
            retorno_tringulo = '\nOs lados fornecidos são de um isósceles.'
        elif x != y and x != z and y != z:
            retorno_tringulo = '\nOs lados fornecidos são de um escaleno.'
        return retorno_tringulo, True
    
    #Negativa do tipo de triângulo
    else:
        retorno_tringulo = '\nOs lados fornecidos não se encaixam nas classificações de triângulos.'
        return retorno_tringulo, False

#Receptor de dados do usuário
x = int(input('Tamanho de X: '))
y = int(input('Tamanho de Y: '))
z = int(input('Tamanho de Z: '))

#Guardar os valores na tupla
tamanho_lado = (x, y, z)

#Retorno do resultado
print(tipo_triangulo(tamanho_lado)[0])
