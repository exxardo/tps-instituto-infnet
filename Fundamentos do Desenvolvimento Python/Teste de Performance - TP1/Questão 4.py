#Calculo fatorial do valor n
def calculo_fatorial(valor_n):
    resultado = 1
    contador = 1
    while contador <= valor_n:
        resultado *= contador
        contador += 1

    print(f'\nO fatorial de {valor_n} é: {resultado}')

#Receptor do dado do usuário
valor_n = int(input('Digite o valor de n: '))

#Parametro para recusar números negativos
if valor_n >= 0:
    calculo_fatorial(valor_n)
else:
    print('\nNão é possível calcular seu fatorial.')
