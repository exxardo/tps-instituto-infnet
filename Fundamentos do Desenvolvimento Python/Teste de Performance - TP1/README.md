1. Escreva uma função em Python que some todos os números ímpares de 1 até um dado N, inclusive. O número N deve ser obtido do usuário. Ao final, escreva o valor do resultado desta soma.

![Questão 1](https://github.com/exxardo/assets/blob/main/Thonny%20%20-%20%20C__Users_Condominio02_Desktop_Quest%C3%A3o%201.py%20%20%40%20%2011%20_%2028%2009_08_2021%2008_51_32.png)

2. Faça uma função em Python que receba do usuário a idade de uma pessoa em anos, meses e dias e retorne essa idade expressa em dias. Considere que todos os anos têm 365 dias.

   ![Questão 2](https://github.com/exxardo/assets/blob/main/Thonny%20%20-%20%20C__Users_Condominio02_Desktop_Quest%C3%A3o%202.py%20%20%40%20%2011%20_%2028%2009_08_2021%2008_58_51.png)

3. Escreva uma função em Python que calcule o fatorial de um dado número N usando um for. O fatorial de N=0 é um. O fatorial de N é (para N > 0): N x (N-1) x (N-2) x … x 3 x 2 x 1. Por exemplo, para N=5 o fatorial é: 5 x 4 x 3 x 2 x 1 = 120. Se N for negativo, exiba uma mensagem indicando que não é possível calcular seu fatorial.

   ![Questão 3](https://github.com/exxardo/assets/blob/main/Thonny%20%20-%20%20C__Users_Condominio02_Desktop_Quest%C3%A3o%203.py%20%20%40%20%2011%20_%2028%2009_08_2021%2009_03_22.png)

4. Escreva um programa em Python que calcule o fatorial de um dado número N usando um while. Use as mesmas especificações do item anterior.

   ![Questão 4](https://github.com/exxardo/assets/blob/main/Thonny%20%20-%20%20C__Users_Condominio02_Desktop_Quest%C3%A3o%204.py%20%20%40%20%2011%20_%2028%2009_08_2021%2009_07_58.png)

5. Trabalhar com tuplas é muito importante! Crie 4 funções nas quais:
   - Dada uma tupla e um elemento, verifique se o elemento existe na tupla e retorne o indice do mesmo
   - Dada uma tupla, retorne 2 tuplas onde cada uma representa uma metade da tupla original.
   - Dada uma tupla e um elemento, elimine esse elemento da tupla.
   - Dada uma tupla, retorne uma nova tupla com todos os elementos invertidos.

6. Escreva um programa em Python que receba três valores reais X, Y e Z, guarde esses valores numa tupla e verifique se esses valores podem ser os comprimentos dos lados de um triângulo e, neste caso, retorne qual o tipo de triângulo formado. Para que X, Y e Z formem um triângulo é necessário que a seguinte propriedade seja satisfeita: o comprimento de cada lado de um triângulo deve ser menor do que a soma do comprimento dos outros dois lados. Além disso, o programa deve identificar o tipo de triângulo formado observando as seguintes definições:

   - Triângulo Equilátero: os comprimentos dos três lados são iguais.

   - Triângulo Isósceles: os comprimentos de dois lados são iguais.

   - Triângulo Escaleno: os comprimentos dos três lados são diferentes.

![Questão 6](https://github.com/exxardo/assets/blob/07605b3c5b38d43c3646264da239dca2c14f859b/Thonny%20%20-%20%20C__Users_Condominio02_Desktop_Quest%C3%A3o%206.py%20%20@%20%2011%20_%2028%2009_08_2021%2009_15_04.png)

7. Escreva uma função que receba uma string e um número inteiro x e rotacione a string x posições para a esquerda. Assuma que a string tem pelo menos x caracteres. Isto é, utilizando como entradas a string “aeiou” e o inteiro 3, o resultado da sua função deve ser “ouaei”.

![Questão 7](https://github.com/exxardo/assets/blob/main/Thonny%20%20-%20%20C__Users_Condominio02_Desktop_Quest%C3%A3o%207.py%20%20%40%20%2011%20_%2028%2009_08_2021%2009_22_37.png)
