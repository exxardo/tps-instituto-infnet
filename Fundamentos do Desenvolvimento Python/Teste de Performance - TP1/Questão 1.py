#Função responsável por somar os termos ímpares
def somar_impares(total_termos):
    soma = 0
    for termo in range(1, total_termos, 2):
        soma += termo
    print(f'\nA soma dos termos ímpares é {soma}')

#Receptor do dado do usuário
total_termos = int(input('Digite o número de termos: '))

somar_impares(total_termos)
