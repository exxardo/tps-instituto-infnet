#Função que calcula o total de dias
def calculo_dias(idade_anos, idade_meses, idade_dias):

    idade_anos_em_dias = idade_anos * 365
    total_de_dias = idade_anos_em_dias + idade_dias
    
    #Converter meses em dias
    meses = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    dias_do_mes = 0
    for mes in range(idade_meses):
        dias_do_mes += meses[mes]
    
    #Soma do total de dias com a conversão dos meses em dias
    total_de_dias += dias_do_mes
    
    #Resultado
    print(f'Você viveu {total_de_dias} dias!')

#Receptor de dados do usuário
idade_anos = int(input('Insira quantos anos você tem: '))
idade_meses = int(input('Resto da sua idade em meses: '))
idade_dias = int(input('Resto da sua idade em dias: '))

#Envio de dados para a função
calculo_dias(idade_anos, idade_meses, idade_dias)
