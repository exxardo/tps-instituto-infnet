while True:
    try:
        idade = int(input("Informe a idade: "))
        if idade <1:
            print("\033[;1mIdade deve ser superior a 1.\033[m\n")
        else:
            if idade >= 18 and idade < 70:
                print("\033[1;34mTem obrigação de votar.\033[m\n")
            elif idade == 16 or idade == 17 or idade >= 70:
                print("\033[1;32mNão tem obrigação de votar.\033[m\n")
            else:
                if idade < 16:
                    print("\033[0;31mNão tem direito a voto.\033[m\n")
        print("----------------------------------------------------------------------------------")
        prossegue = input('Pressione "\033[0;31mENTER\033[m" para continuar ou "\033[0;31mF\033[m" para finalizar. ').upper()
        if prossegue == 'F':
            print('Finalizando...\n\nFim!')
            break
        else:
            print("----------------------------------------------------------------------------------")
            continue
    except ValueError:
        print("\n\033[0;31mOpa! Algo deu errado.\033[m\n----------------------------------------------------------------------------------\nVamos recomeçar.\n")
