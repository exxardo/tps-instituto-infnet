while True:    
    try:
        quantidade_part = int(input("Quantos participantes teve o concurso? "))
        for contador in range(quantidade_part):
            participante = input(f"Informe nome do {contador + 1}º participante: ")
            nota = float(input(f"Informe nota do {contador + 1}º participante: "))
            if nota >=0  and nota <=10:
                if contador == 0 or nota > nota_ganhadora:
                    ganhador = participante
                    nota_ganhadora = nota

            while nota <0 or nota > 10:
                print('\n\033[0;31mA nota deve ser maior que "0" e menor que "10"\033[m \n')
                nota = float(input(f"Informe nota do {contador + 1}º participante: "))
                if nota >=0  and nota <=10:
                    if contador == 0 or nota > nota_ganhadora:
                        ganhador = participante
                        nota_ganhadora = nota
        print(f"\nO(a) vencedor(a) foi {ganhador} com nota {nota_ganhadora}!")
        print("----------------------------------------------------------------------------------")
        prossegue = input('Pressione "\033[0;31mENTER\033[m" para continuar ou "\033[0;31mF\033[m" para finalizar. ')
        if prossegue == 'f' or prossegue == 'F':
            print('Finalizando...\n\nFim!')
            break
        else:
            print("----------------------------------------------------------------------------------")
            continue
    except ValueError:
        print('\033[0;31mO caracter digitado é inválido.\033[m \n')
        prossegue2 = input('Pressione "\033[0;31mENTER\033[m" para reiniciar ou "\033[0;31mF\033[m" para finalizar. ')
        if prossegue2 == 'f' or prossegue2 == 'F':
            print('Finalizando...\n\nFim!')
            break
        else:
            print("----------------------------------------------------------------------------------")
            continue
