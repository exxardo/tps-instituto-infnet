while True:
    try:
        total = float(input("Informe o valor total do consumo: R$ "))
        if total > 0:
            pessoas = int(input("Informe o total de pessoas: "))
            if pessoas >= 1:
                percentual = float(input("Informe o percentual do serviço, entre 0 e 100: "))
                if percentual >=0 and percentual <=100:
                    totalConsumo = float(total + (total * percentual / 100))
                    totalPessoa = float(totalConsumo / pessoas)
                    totalConsumo = ("%0.2f" % totalConsumo).replace(".", ",")
                    totalPessoa = ("%0.2f" % totalPessoa).replace(".", ",")
                    print(f"O valor total da conta, com a taxa de serviço, será de R$ {totalConsumo}.\n")
                    print(f"\033[1mDividindo a conta por {pessoas} pessoa(s), cada pessoa deverá pagar R$ {totalPessoa}.\033[m")
                else:
                    print('\033[0;31mA porcentagem deve ser entre "0" e "100".\033[m')
            else:
                print("\033[0;31mValor inválido. Necessário ao menos uma pessoa.\033[m")
        else:
            print("\033[0;31mVocê precisa ter consumido!\033[m")
        print("\n----------------------------------------------------------------------------------")
        prossegue = input('Pressione "\033[0;31mENTER\033[m" para continuar ou "\033[0;31mF\033[m" para finalizar. ')
        if prossegue == 'f' or prossegue == 'F':
            print('Finalizando...\n\nFim!')
            print("----------------------------------------------------------------------------------")
            break
    except ValueError:
        print("\033[0;31m \nEntrada inválida\033[m \n")
