while (notDone()) {
	if (isPathForward()) {
		moveForward();
	}
	if (isPathRight()) {
		turnRight();
	}
}