while (notDone()) {
	if (isPathLeft()) {
		turnLeft();
	}
	moveForward();
	if (isPathLeft()) {
		turnLeft();
	}
	moveForward();
	if (isPathRight()) {
		turnRight();
		moveForward();
	}
}
