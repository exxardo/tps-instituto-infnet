# TESTE DE PERFORMANCE - TP1 OBRIGATÓRIO

## DOCUMENTAÇÃO REFERENTE AO TESTE DE PERFORMANCE

------

### Programando com Google Blockly

------
#### Labirinto

##### **Etapa explicativa**:

Para cada animal (verde), anexe sua imagem, escolha seu numero de pernas, e faça uma lista de seus traços.

Não foi necessário linhas de código.

##### **Etapa 1: Empilhe alguns blocos 'avançar' juntos para me ajudar a alcançar o objetivo.**

`avançar`

`avançar`

```javascript
moveForward();
moveForward();
```

##### **Etapa 2:**

`avançar`

`vire à esquerda`

`avançar`

`vire à direita`

`avançar`

```javascript
moveForward();
turnLeft();
moveForward();
turnRight();
moveForward();
```

##### **Etapa 3: Alcance o fim deste caminho usando apenas dois blocos. Use 'repetir' para executar um bloco mais de uma vez.**

`enquanto não chega no ponto`

- `avançar`

```javascript
while (notDone()) {
 	moveForward();
}
```

##### **Etapa 4: Você pode encaixar mais de um bloco dentro de um bloco 'repetir'.**

`enquanto não chega no ponto`

- `avançar`
- `vire à esquerda`
- `avançar`
- `vire à direita`

```javascript
while (notDone()) {
	moveForward();
	turnLeft();
	moveForward();
	turnRight();
}
```

##### **Etapa 5:**

`avançar`

`avançar`

`vire à esquerda`

`enquanto não chega no ponto`

- `avançar`

```javascript
moveForward();
moveForward();
turnLeft();
while (notDone()) {
	moveForward();
}
```

##### **Etapa 6: Um bloco 'se' fará alguma coisa apenas se a condição for verdadeira. Tente virar à esquerda se houver um caminho para a esquerda.**

`enquanto não chega no ponto`

- `avançar`
- `se caminho à esquerda faça`
  - `vire à esquerda`

```javascript
while (notDone()) {
	moveForward();
	if (isPathLeft()) {
		turnLeft();
	}
}
```

##### **Etapa 7:**

`enquanto não chega no ponto`

- `se caminho à frente faça`
  - `avançar`

- `se caminho à direita faça`
  - vire à direita

```javascript
while (notDone()) {
	if (isPathForward()) {
		moveForward();
	}
	if (isPathRight()) {
		turnRight();
	}
}
```

##### **Etapa 8**:

- `enquanto não chega no ponto`
  - `avançar`
- `se caminho à direita faça`
  - `vire à direita`
- `se caminho à esquerda faça`
  - `vire à esquerda`

```javascript
while (notDone()) {
	if (isPathForward()) {
		moveForward();
	}
	if (isPathRight()) {
		turnRight();
	}
	if (isPathLeft()) {
		turnLeft();
	}
}
```

##### **Etapa 9: Blocos se-senão farão uma coisa ou outra.**

- `enquanto não chega no ponto`
  - `se caminho à frente faça`
    - `avançar`
  - `senão`
    - `se caminho à esquerda faça`
      - `vire à esquerda`
    - `se caminho à direita faça`
      - `vire à direita`

```javascript
while (notDone()) {
	if (isPathForward()) {
		moveForward();
	} else {
		if (isPathLeft()) {
			turnLeft();
		}
		if (isPathRight()) {
			turnRight();
		}
	}
}
```

##### **Etapa 10:** 

`enquanto não chega no ponto`

- `se caminho à frente faça`
  - `avançar`
- `senão`
  - `se caminho à esquerda faça`
    - `vire à esquerda`
    - `se caminho à direita faça`
      - `vire à direita`

O código não funcionou. O boneco passou da primeira entrada e entrou em um loop no fim do caminho.

Tentativa 2:

`enquanto não chega no ponto`

- `avançar`

- `se caminho à direita faça`
  - `vire à direita`
- `senão`
  - `se caminho à esquerda faça`
    - `vire à esquerda`

Não funcionou. Boneco andou até a parte final, porém andou virou para o lado contrário.

Tentativa 3:

`enquanto não chega no ponto`
- `se caminho à esquerda faça`
	- `vire à esquerda`
- `avançar`
- `se caminho à esquerda faça`
	- `vire à esquerda`
- `avançar`
- `se caminho à direita faça`
  - `vire à direita`
  - `avançar`

```javascript
while (notDone()) {
	if (isPathLeft()) {
		turnLeft();
	}
	moveForward();
	if (isPathLeft()) {
		turnLeft();
	}
	moveForward();
	if (isPathRight()) {
		turnRight();
		moveForward();
	}
}
```

Código funcionou corretamente.
------

#### Pássaro

##### **Etapa 1:** 
Consitia em girar o passáro no ângulo correto, afim de o levar até o ninho.

`direção 45°`

```javascript
heading(45);
```
##### **Etapa 2:** 
