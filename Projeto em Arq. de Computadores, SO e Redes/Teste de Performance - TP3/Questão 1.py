import pygame
import psutil
import cpuinfo
import platform

# Cores
azul = (65, 105, 225)
vermelho = (139, 0, 0)
preto = (0, 0, 0)
branco = (255, 255, 255)
cinza = (100, 100, 100)
verde = (0, 128, 0)


def mostra_uso_memoria():
    mem = psutil.virtual_memory()
    larg = largura_tela - 30 * 20
    # Superficies
    tela.blit(superficie_memoria, (550, 146))
    # Superficies
    pygame.draw.rect(superficie_memoria, azul, (15, 100, largura_tela - 30 * 20, 70))
    larg = larg * mem.percent / 100
    # Superficies
    pygame.draw.rect(superficie_memoria, vermelho, (15, 100, larg, 70))

    total = round(mem.total / (1024 * 1024 * 1024), 2)
    usado = round(mem.used / (1024 * 1024 * 1024), 2)
    disponivel = round(mem.available / (1024 * 1024 * 1024), 2)
    texto_barra = f'Memória Total: {total} GB | Disponível: {disponivel} GB | Utilizado: {usado} GB ({mem.percent}%)'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (565, 220))


def mostra_uso_disco():
    disco = psutil.disk_usage('.')
    larg = largura_tela - 30 * 20
    # Superficies
    tela.blit(superficie_disco, (550, 416))
    # Superficies
    pygame.draw.rect(superficie_disco, azul, (15, 100, largura_tela - 30 * 20, 70))
    larg = larg * disco.percent / 100
    # Superficies
    pygame.draw.rect(superficie_disco, vermelho, (15, 100, larg, 70))

    total = round(disco.total / (1024 * 1024 * 1024), 2)
    usado = round(disco.used / (1024 * 1024 * 1024), 2)
    disponivel = round(disco.free / (1024 * 1024 * 1024), 2)
    texto_barra = f'Tamanho Total: {total} GB | Disponível: {disponivel} GB | Utilizado: {usado} GB ({disco.percent}%)'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (565, 490))


def mostra_uso_cpu_total():
    capacidade = psutil.cpu_percent(interval=0)
    larg = largura_tela - 40 * 20
    # Superficies
    tela.blit(superficie_cpu, (10, 416))
    # Superficies
    pygame.draw.rect(superficie_cpu, azul, (10, 100, largura_tela - 39 * 20, 70))
    larg = larg * capacidade / 100
    # Superficies
    pygame.draw.rect(superficie_cpu, vermelho, (10, 100, larg, 70))
    text = font.render(f'Utilização total da CPU: {capacidade}%', 1, branco)
    tela.blit(text, (20, 490))


def mostra_uso_cpu(s, l_cpu_percent):
    s.fill(preto)
    num_cpu = len(l_cpu_percent)
    x = 10
    y = 10
    desl = 20
    alt = s.get_height() - 43 * y
    larg = (s.get_width() - 76 * y - (num_cpu + 1) * desl) / num_cpu
    d = x + desl
    for i in l_cpu_percent:
        pygame.draw.rect(s, vermelho, (d, y, larg, alt))
        pygame.draw.rect(s, verde, 	(d, y, larg, (1 - i / 100) * alt))
        d = d + larg + desl
    # parte mais abaixo da tela e à esquerda
    tela.blit(s, (0, altura_tela / 5))


# Mostra texto de acordo com uma chave:
def mostra_texto(superficie_info_cpu, nome, chave, pos_y):
    text = font.render(nome, True, preto)
    superficie_info_cpu.blit(text, (10, pos_y))
    if chave == "freq":
        s = str(round(psutil.cpu_freq().current, 2))
    elif chave == "nucleos":
        s = str(psutil.cpu_count())
        s = s + " (" + str(psutil.cpu_count(logical=False)) + ")"
    else:
        s = str(info_cpu[chave])
    text = font.render(s, True, cinza)
    superficie_info_cpu.blit(text, (160, pos_y))


def mostra_info_cpu():
    superficie_info_cpu.fill(branco)
    mostra_texto(superficie_info_cpu, "Nome:", "brand_raw", 10)
    mostra_texto(superficie_info_cpu, "Arquitetura:", "arch", 30)
    mostra_texto(superficie_info_cpu, "Palavra (bits):", "bits", 50)
    mostra_texto(superficie_info_cpu, "Frequência (MHz):", "freq", 70)
    mostra_texto(superficie_info_cpu, "Núcleos (físicos):", "nucleos", 90)
    tela.blit(superficie_info_cpu, (0, 0))


# def mostra_info_memoria():
def mostra_info_ip():
    superficie_info_ip_mac.fill(branco)
    dic_interfaces = psutil.net_if_addrs()
    ip_maquina = dic_interfaces['Conexão Local* 1'][1].address
    texto_barra = f'Endereço da máquina:    {ip_maquina}'
    text = font.render(texto_barra, 1, preto)
    tela.blit(text, (650, 10))


def mostra_info_mac():
    superficie_info_ip_mac.fill(branco)
    dic_interfaces = psutil.net_if_addrs()
    mac_maquina = dic_interfaces['Conexão Local* 1'][0].address
    texto_barra = f'MAC:                                      {mac_maquina}'
    text = font.render(texto_barra, 1, preto)
    tela.blit(text, (650, 30))


def datalhar_plataforma():
    superficie_info_ip_mac.fill(branco)
    plataforma = platform.platform()
    text = font.render(plataforma, 1, preto)
    tela.blit(text, (650, 50))


def datalhar_processador():
    superficie_info_ip_mac.fill(branco)
    processador = platform.processor()
    text = font.render(processador, 1, preto)
    tela.blit(text, (650, 70))


# Obtém informações da CPU
info_cpu = cpuinfo.get_cpu_info()

# Iniciando a janela principal
largura_tela = 1300
altura_tela = 680
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption('Monitor de recursos do computador')
pygame.display.init()
# Superfície para mostrar as informações:
superficie_info_cpu = pygame.surface.Surface((largura_tela, altura_tela))
superficie_info_ip_mac = pygame.surface.Surface((largura_tela, altura_tela - 550))
# Para usar na fonte
pygame.font.init()
font = pygame.font.Font(None, 24)

# Cria relógio
clock = pygame.time.Clock()
# Contador de tempo
cont = 60

superficie_memoria = pygame.surface.Surface((730, 250))
superficie_disco = pygame.surface.Surface((730, 250))
superficie_cpu = pygame.surface.Surface((535, 250))

terminou = False
# Repetição para capturar eventos e atualizar tela
while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True

    # Fazer a atualização a cada segundo:
    if cont == 60:
        mostra_info_cpu()
        mostra_uso_cpu(superficie_info_cpu, psutil.cpu_percent(interval=1, percpu=True))
        mostra_info_ip()
        mostra_info_mac()
        mostra_uso_memoria()
        mostra_uso_disco()
        mostra_uso_cpu_total()
        datalhar_plataforma()
        datalhar_processador()
        cont = 0

    # Atualiza o desenho na tela
    pygame.display.update()

    # 60 frames por segundo
    clock.tick(60)
    cont = cont + 1

# Finaliza a janela
pygame.display.quit()