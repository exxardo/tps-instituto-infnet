[TOC]

# TESTE DE PERFORMANCE - TP3

**INSTITUIÇÃO**: INSTITUTO INFNET

**DISCIPLINA**: PROJETO EM ARQUITETURA DE COMPUTADORES, SISTEMAS OPERACIONAIS E REDES

**TRIMESTRE**: 21E3 - 21E4

**PROFESSOR**: ALCIONE DOLAVALE

**ALUNO**: EDUARDO RAMIRO DE MATOS

-----

### RELATÓRIO - MONITOR DE RECURSOS DO COMPUTADOR

Neste TP, você tem liberdade para criar quantas superfícies achar necessárias. Além disso, pode aumentar as dimensões da tela e alterar as dimensões e posições das superfícies conforme achar melhor.

Adicione ao seu relatório feito no TP2 informações mais detalhadas sobre o entregável. Você pode adicionar as informações da maneira que achar mais interessante. No entanto, existem itens **obrigatórios**:

- Descreva de maneira detalhada o entregável.

- Descreva de maneira teórica (pode utilizar exemplos) as diferenças de arquitetura de CPU.

- Descreva de maneira teórica o que é a palavra do processador.

- Descreva de maneira teórica a diferença entre os núcleos físicos e lógicos. E em que influencia no processamento a utilização de núcleos lógicos.

O que deve ser entregue:

- Relatório explicativo sobre o entregável.

- Ainda, no mesmo relatório,o aluno deve apresentar todos os artefatos como código do programa junto com impressões da tela (printscreen) do resultado de uma ou mais execuções que mostrem que seu programa funciona conforme o requisitado.

- Adicionalmente, o arquivo .py com o código do programa.

----

#### Sobre o entregável:

O programa tem como função o monitoramente e exibição dos recursos presentes no computador. Ele se utiliza dos seguintes módulos e ferramentas: pygame, psutil, py-cpuinfo e platform.
Tais módulos juntos conferem ao programa uma boa visualização do consumo de memória ram, a utilização do processador, do armazenado, além de apresentar informações detalhadas quanto ao processador, a versão do sistema operacional e o endereço de IP da máquina.

Com a utilização do programa é possível capturar em tempo real as informações sobre o estado do computador.

#### Capturas de tela:

![CAP 1](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-09-12%20212439.png)

![CAP 2](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-09-12%20212532.png)

![CAP 3](https://github.com/exxardo/assets/blob/main/Captura%20de%20tela%202021-09-12%20213354.png)

----

#### Sobre os módulos e ferramentas utilizados:

##### `pygame`

Pygame é uma biblioteca de jogos que utlizamos em conjunto com o Python. Utilizamos esse módulo para criar a interface gráfica do programa: a janela, as barras de nível e a plotagem das informações na janela. A plotagem das informações na janela se deu através do usode um recurso do pygame chamado: surface. Surfaces são as superfícies em 2D (que podem ser também 3D) onde se desenha o as informções, podendo preencher uma área com uma cor ou mudar a cor da superfície dependendo da posição, e outros recursos.

Exemplo do uso da surface:
```PYTHON
superficie_memoria = pygame.surface.Surface((730, 250))
superficie_disco = pygame.surface.Surface((730, 250))
superficie_cpu = pygame.surface.Surface((535, 250))
```

##### `psutil`

Psutil é uma biblioteca de plataforma cruzada para recuperar informações sobre os processos em execução e a utilização do sistema como CPU, memória, discos, rede e sensores em Python. É útil no monitoramento de sistema. Utilizamos o psutil na captura das informações referentes ao uso da memória, ao uso do procesador e no monitoramente do memória de armazenamento.

Podemos ver o uso do Psutil na seguinte função:

```PYTHON
def mostra_uso_memoria():
    mem = psutil.virtual_memory()
    larg = largura_tela - 30 * 20
    # Superficies
    tela.blit(superficie_memoria, (550, 146))
    # Superficies
    pygame.draw.rect(superficie_memoria, azul, (15, 100, largura_tela - 30 * 20, 70))
    larg = larg * mem.percent / 100
    # Superficies
    pygame.draw.rect(superficie_memoria, vermelho, (15, 100, larg, 70))

    total = round(mem.total / (1024 * 1024 * 1024), 2)
    usado = round(mem.used / (1024 * 1024 * 1024), 2)
    disponivel = round(mem.available / (1024 * 1024 * 1024), 2)
    texto_barra = f'Memória Total: {total} GB | Disponível: {disponivel} GB | Utilizado: {usado} GB ({mem.percent}%)'
    text = font.render(texto_barra, 1, branco)
    tela.blit(text, (565, 220))
```

##### `cpuinfo`

Py-cpuinfo obtém informações da CPU, como nomeclatura, núcleos, etc. utilizando o Python puro. Neste projeto ele foi usado  para capturar informações detalhadas do processador, a fim entregar mais detalhes. 

Como vemos na terceira linha da função `mostra_uso_cpu()`: 

```PYTHON
def mostra_texto(superficie_info_cpu, nome, chave, pos_y):
    text = font.render(nome, True, preto)
    superficie_info_cpu.blit(text, (10, pos_y))
    if chave == "freq":
        s = str(round(psutil.cpu_freq().current, 2))
    elif chave == "nucleos":
        s = str(psutil.cpu_count())
        s = s + " (" + str(psutil.cpu_count(logical=False)) + ")"
    else:
        s = str(info_cpu[chave])
    text = font.render(s, True, cinza)
    superficie_info_cpu.blit(text, (160, pos_y))


def mostra_info_cpu():
    superficie_info_cpu.fill(branco)
    mostra_texto(superficie_info_cpu, "Nome:", "brand_raw", 10)
    mostra_texto(superficie_info_cpu, "Arquitetura:", "arch", 30)
    mostra_texto(superficie_info_cpu, "Palavra (bits):", "bits", 50)
    mostra_texto(superficie_info_cpu, "Frequência (MHz):", "freq", 70)
    mostra_texto(superficie_info_cpu, "Núcleos (físicos):", "nucleos", 90)
    tela.blit(superficie_info_cpu, (0, 0))
```

##### `platform`

Com este móduto é possível obter características do processador, como o nome e modelo. Além disso, estão disponíveis também informações sobre o sistema operacional. Utilizamos ele para capturar a informação sobre a versão do sistemas operacional.

Exemplo de utilização em duas funções:

```PYTHON
def datalhar_plataforma():
    superficie_info_ip_mac.fill(branco)
    plataforma = platform.platform()
    text = font.render(plataforma, 1, preto)
    tela.blit(text, (650, 50))


def datalhar_processador():
    superficie_info_ip_mac.fill(branco)
    processador = platform.processor()
    text = font.render(processador, 1, preto)
    tela.blit(text, (650, 70))
```

----

#### **Descreva de maneira teórica (pode utilizar exemplos) as diferenças de arquitetura de CPU.**

Na atualidade é muito comum vermos softwares com versões 32 bits e 64 bits, isso ocorre devido a atual arquitetura dos computadores modernos e por isso é comum que os softwares tenham versões compatíveis com tal.

Os computadores de 32 bits possuem a capacidade de processar “palavras”, que nada mais são do que sequência de bits. Os computadores de 64 bits possuem o dobro da capacidade para processar informações, ou seja, podem trabalhar aquelas com até 64 bits.

Quando se trata de sistema operacional, os processadores de 64 bits conseguem trabalhar com mais memória RAM. A exemplo temos o Windows 7, que suportava memórias de até 4 GB (contra até 192 GB das arquiteturas 64 bits).

Nesse mundo de processadores, temos também as arquiteturas ARM e x86. Na prática, a diferença entre os dois se dá na complexidade: o ARM é baseado na RISC (Reduced Instruction Set Computer) e a x86 na CISC (Complex Instruction Set Computing). Os processadores ARM operam apenas em registradores, enquanto o x86 pode operar também na memória.

Atualmente a arquitetura 64 bits é a mais utilizada, tanto em smartphones, quanto em computadores.

#### **Descreva de maneira teórica o que é a palavra do processador.**

Palavra é um termo da Ciência da Computação para unidade natural de informação e cada computador a utiliza de uma forma, e nada mais é que a sequência de bits que a máquina processa.

Os computadores da atualidade possuem dois tamanhos de palavra: 32 bits e 64 bits. A depender de como o computador é organizado, essa unidade natural pode ser usada para números inteiros, números de pontos flutuantes e endereços

#### **Descreva de maneira teórica a diferença entre os núcleos físicos e lógicos. E em que influencia no processamento a utilização de núcleos lógicos.**

Os núcleos físicos são literalmente a parte física dentro da CPU, contando com núcleos distintos em um mesmo circuito integrado, enquanto os lógicos consistem na capacidade dos físicos para fazer mais de um trabalho simultaneamente. Cada processador lógico vai receber seu próprio controlador e um conjunto de regitradores.

Os núcleos lógicos agregam mais desempenho à máquina, permitindo o sistema operacional enviar tarefas para eles como se estivessem enviando para núcleos físicos. É como se um núcleo físico trabalha-se por dois.

-----



